package com.example.testview;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import android.os.Bundle;
import android.os.Message;

public class ScInThread extends SimpleClient implements Runnable 
{
	//private boolean conn=false;
	private String msg=null;
	
	public ScInThread(String ip,int s)
	{
		super(ip,s);
	}
   public SocketReader sr;
    public boolean SClose()
    {
    	try{
    	    s.close();
    	    Global.conn=false;
    	    return true;
    	}
    	catch(Exception e){
    	    boolean b=super.SClose();
    	    if(b)
    	    Global.conn=false;
    	    return b;
    	}
    }
	public void setMsg(String str)
	{
		msg=str;
	}
	public boolean isConn()
	{
		return s.isConnected();
	}
	public boolean SetUp()
	{
		boolean c=super.SetUp();
		if(c)
		{
			sr=new SocketReader(s);
			Global.conn=true;
			if(!sr.isAlive())
		    sr.start();
		    Global.callUI(0, "conn", "Connected");
		}
		else
			Global.callUI(0, "conn", "Failed");
		return c;
	}

	public void run()
	{
		if(Global.conn)
		{
			if(msg!=null)
			{
				if(this.SendMsg(msg))
				Global.callUI(1,"send",msg);
				else
					Global.callUI(1,"send","senError");
					
			}
		}
		else
		{
		    Global.conn=this.SetUp();
		   
		 }
	}
}
