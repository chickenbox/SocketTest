package com.example.testview;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.R.integer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements Callback{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Global.myhandler=new Handler(this);
		btnConn=(Button)findViewById(R.id.buttonConn);
		tvState=(TextView)findViewById(R.id.connState);
		tvMsg=(TextView)findViewById(R.id.textViewM);
		etm=(EditText)findViewById(R.id.editTextMsg);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public int sPort=3030;
	public String IpAdr="192.168.1.105";
	public TextView tvState;
	public TextView tvMsg;
	public EditText etm;
	public boolean Conn=false;
	public static ExecutorService pool=Executors.newSingleThreadExecutor();
	public ScInThread sc;
	public Button btnConn;
	public void ConnOnClick(View v)
	{
		if(Global.conn)
		{
			if(sc.SClose())
			{
				btnConn.setText("Connect");
			}
		}
		else
		{
		    EditText eti=(EditText)findViewById(R.id.editTextIP);
		    EditText etp=(EditText)findViewById(R.id.editTextPort);
		    if(eti.getText().toString().length()>0)
		    {
		    sPort=Integer.parseInt(etp.getText().toString());
		    IpAdr=eti.getText().toString();
		    }
		    sc=new ScInThread(IpAdr,sPort);
		    pool.execute(sc);
		}
		
	}
	
	public void SendOnClick(View v)
	{
		if(Global.conn)
		{
			sc.setMsg(etm.getText().toString());
			pool.execute(sc);
		}
		else
		{
		
		    tvState.setText("Not Coonect");
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		// TODO Auto-generated method stub
		Editable ea=(Editable)tvMsg.getText();
		switch(msg.what)
		{
		case 0:{
			tvState.setText(msg.getData().getString("conn"));
			if(Global.conn)
				btnConn.setText("DisConnect");	
			break;
		}
		case 1:{
			String sKey="send";
			ea.insert(0,sKey+":"+msg.getData().getString(sKey)+"\n");
			etm.setText(null);
			break;
		}
		case 2:{
			String rKey="recv";
			String mm=msg.getData().getString(rKey);
			ea.insert(0,rKey+":"+mm+"\n");
			if(mm.equals("Error"))
			{
				if(Global.conn)
				{
				    sc.SClose();
				    if(!Global.conn)
				    {
					btnConn.setText("Connect");
					Global.conn=false;
				    }
				}
			}
			break;
		}
		}
		return false;
	}
}
