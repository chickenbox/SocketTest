package com.example.testview;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

public class Global {
	public static boolean conn=false;
	public static Handler myhandler;
	public static void callUI(int what,String key,String text)
	{
		Message msg =new Message();
		msg.what=what;
		Bundle data =new Bundle();
		data.putString(key,text);
		msg.setData(data);
		myhandler.sendMessage(msg);
		
	}

}
