package com.example.testview;

import java.net.*;
import java.io.*;

public class SimpleClient {
	private int sPORT=3030;
	//public String endflag="/r/n";
	protected Socket s;
	private String ipAddr="192.168.1.105";
	private DataOutputStream dout;
	private boolean conn;
	public SimpleClient(String ip,int sPort)
	{
		this.ipAddr=ip;
		this.sPORT=sPort;
		
	}
	public boolean SetUp()
	{
		try{
		s=new Socket(ipAddr,sPORT);
		if(s.isConnected())
		{
			//System.out.println(s.getInetAddress().toString());
			conn=true;
			return true;
		}
		else return false;
		}catch(Exception e)
		{
			return false;
			//System.out.println(e.getStackTrace()+"/setup");
		}
	}
	
	public boolean SendMsg(String str)
	{
		if(s.isConnected())
		{
			try{
			dout=new DataOutputStream(s.getOutputStream());
			dout.writeUTF(str);
			return true;
			//dout.writeUTF(endflag);		
			}catch(Exception e)
			{
				return false;
				//System.out.println(e.getStackTrace()+"/sendcom");
			}
		}
		return false;
	}
	public boolean SClose()
	{
		try{
		s.shutdownOutput();
		dout.close();
		s.close();
		return true;
		}catch(Exception e)
		{
			return false;
			//System.out.println("socketClient closing error");		
		}
	}

}
