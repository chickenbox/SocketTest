package com.example.testview;

import java.io.DataInputStream;
import java.net.Socket;

public class SocketReader extends Thread
{
	private DataInputStream din;
	private String msg;
	private Socket s;
	public SocketReader(Socket s)
	{
		this.s=s;
		
	}
	
	public boolean Close()
	{
		try{
		s.shutdownInput();
		din.close();
		return true;
		}catch(Exception e)
		{
			return false;
		}
	}
	public void run()
	{
		String str;
		try{	
		din=new DataInputStream(s.getInputStream());
		while((str=din.readUTF())!=null)
		{
			msg=str;
			Global.callUI(2,"recv",msg);
			//Global.tv.setText(msg);
		}
		}catch(Exception e)
		{
			//System.out.println(e.getStackTrace()+"/read");
			//Global.tv.setText("Error");
			Global.callUI(2, "recv", "Error");
		}
	}

}
